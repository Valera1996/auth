﻿var config = require('config.js');
var express = require('express');
var faker = require('faker');
var router = express.Router();
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
module.exports = router;

// routes
// router.post('/register', registerUser);
router.get('/me1', getCurrentUser);
// router.put('/:_id', updateUser);
// router.delete('/:_id', deleteUser);
// router.get('/', getAll);

router.get('/me', me);

function me (req, res) {
    res.send(req.user);
};

// var user = {
//     username: 'kentcdodds',
//     password: 'p'
// };
// router.post('/login', authenticate, function (req, res) {
//     var token = jwt.sign({
//         username: user.username
//     }, config.secret);
//     res.send({
//         token: token,
//         user: user
//     });
// });
// function authenticate(req, res, next) {
//     var body = req.body;
//     if (!body.username || !body.password) {
//         res.status(400).end('Must provide username or password');
//     } else if (body.username !== user.username || body.password !== user.password) {
//         res.status(401).end('Username or password incorrect');
//     } else {
//         next();
//     }
// };

router.post('/login', authenticate);


function authenticate (req, res) {
    var body = req.body;

    User.authenticate(body.username, body.password)
        .then(function (token) {
            if (token) {
                // authentication successful
                res.send({ token: token, user: body });
            } else {
                // authentication failed
                res.status(401).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
};


// UTIL FUNCTIONS

// function getAll (req,res) {
//
//     var page = +req.query['page'];
//     var limit = +req.query['limit'];
//
//     User.paginate({},{page: page, limit: limit }, function (err, data) {
//         res.send(data);
//     });
// };

// function registerUser(req, res) {
//     User.addUser(req.body)
//         .then(function () {
//             res.sendStatus(200);
//         })
//         .catch(function (err) {
//             res.status(400).send(err);
//         });
// };

function getCurrentUser(req, res) {
    User.getUserById(req.user.sub)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
};

// function updateUser(req, res) {
//
//     User.updateUser(req.params._id, req.body)
//         .then(function () {
//             res.sendStatus(200);
//         })
//         .catch(function (err) {
//             res.status(400).send(err);
//         });
// };
//
// function deleteUser(req, res) {
//     var id = req.params._id;
//     User._delete(id, function (err, user) {
//         if(err){
//             throw err;
//         }
//         res.json(user);
//     })
// };

