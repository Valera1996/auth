require('rootpath')();
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var app = express();
var config = require('./config'); // get our config file
var mongoose = require('mongoose');

app.use(cors());
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(expressJwt({ secret: config.secret }).unless({ path: [ '/login' ]}));
app.use('/', require('./routes/users.controller.js'));
// connect to database
mongoose.connect(config.connectionString);

app.listen(3000, function () {
  console.log('App listening on localhost:3000');
});


