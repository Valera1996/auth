/**
 * Created by vg on 4/28/17.
 */

var app = angular.module('app');

app.factory('userFactory', function userFactory($http, API_URL, AuthInterceptor, $q) {
    'use strict';
    return {
        login: login,
        logout: logout,
        getUser: getUser
    };

    function login(username, password) {
        return $http.post(API_URL + '/login', {
            username: username,
            password: password
        }).then(function success(response) {
            AuthInterceptor.setToken(response.data.token);
            return response;
        });
    };

    function logout() {
        AuthInterceptor.setToken();
    };

    function getUser() {
        if (AuthInterceptor.getToken()) {
            return $http.get(API_URL + '/me');
        } else {
            return $q.reject({ data: 'client has no auth token' });
        }
    };
});
