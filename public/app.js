(function () {
  'use strict';
  var app = angular.module('app', [], function config($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });

  app.constant('API_URL', 'http://localhost:3000');

  app.controller('MainCtrl', function MainCtrl( userFactory) {
    'use strict';
    var vm = this;
    vm.login = login;
    vm.logout = logout;

    // initialization
    userFactory.getUser().then(function success(response) {
      vm.user = response.data;
      console.log(vm.user);
    });

    function login(username, password) {
      userFactory.login(username, password).then(function success(response) {
        vm.user = response.data.user;
      }, handleError);
    };

    function logout() {
      userFactory.logout();
      vm.user = null;
    };

    function handleError(response) {
      alert('Error: ' + response.data);
    };

  });

    app.factory('AuthInterceptor', function AuthInterceptor($window) {
        'use strict';
        var store = $window.localStorage;
        var key = 'auth-token';
        return {
            request: addToken,
            getToken: getToken,
            setToken: setToken
        };

        function getToken() {
            return store.getItem(key);
        };

        function setToken(token) {
            if (token) {
                store.setItem(key, token);
            } else {
                store.removeItem(key);
            }
        };

        function addToken(config) {
            var token = getToken();
            if (token) {
                config.headers = config.headers || {};
                config.headers.Authorization = 'Bearer ' + token;
            }
            return config;
        };
    });


})();
